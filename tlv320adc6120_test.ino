#include <HardwareSerial.h>
#include <Wire.h>
#include "tlv320adcx120_page0.h"
#include "sos-iir-filter.h"
#include "coeff.h"
#include <driver/i2s.h>
#include "movingavg.h"
#include <Preferences.h>
#include "pins.h"
#include <math.h>
//#include <WiFi.h>
#include "wifipassword.h"

uint8_t i2c_addr = 0x4E;
HardwareSerial ll(2);         // use UART2

#define I2S_PORT          I2S_NUM_0
#define I2S_THRU_PORT     I2S_NUM_1

//
// Sampling
//
#define SAMPLE_RATE       48000 // Hz, fixed to design of IIR filters
#define SAMPLE_BITS       32    // bits
#define SAMPLE_T          int32_t
#define SAMPLES_SHORT     (SAMPLE_RATE / 8) // ~125ms / 6000
#define SAMPLES_LEQ       (SAMPLE_RATE * LEQ_PERIOD)
#define DMA_BANK_SIZE     (SAMPLES_SHORT / 16) *2         // 
#define DMA_BANKS         16

#define OVER_VAL          8292584     // after convert to floats, this is the maxvalue the converter will output!
//
// Configuration
//

#define LEQ_PERIOD        1         // second(s)
#define LEQ_UNITS         "LAeq"      // customize based on above weighting used
#define DB_UNITS          "dBA"       // customize based on above weighting used
#define USE_DISPLAY       0

// NOTE: Some microphones require at least DC-Blocker filter
#define MIC_EQUALIZER     None        // See below for defined IIR filters or set to 'None' to disable
#define MIC_OFFSET_DB     3.0103      // Default offset (sine-wave RMS vs. dBFS). Modify this value for linear calibration
//#define MIC_OFFSET_DB     0
// Customize these values from microphone datasheet
#define MIC_SENSITIVITY   -26         // dBFS value expected at MIC_REF_DB (Sensitivity value from datasheet)
#define MIC_REF_DB        94.0        // Value at which point sensitivity is specified in datasheet (dB)
#define MIC_OVERLOAD_DB   116.0       // dB - Acoustic overload point
#define MIC_NOISE_DB      29          // dB - Noise floor
#define MIC_BITS          24          // valid number of bits in I2S data
#define MIC_CONVERT(s)    (s >> (SAMPLE_BITS - MIC_BITS))
#define MIC_TIMING_SHIFT  0           // Set to one to fix MSB timing for some microphones, i.e. SPH0645LM4H-x


#define I2S_TASK_PRI   4
#define I2S_TASK_STACK 4096


// Calculate reference amplitude value at compile time
constexpr double MIC_REF_AMPL = pow(10, double(MIC_SENSITIVITY) / 20) * ((1 << (MIC_BITS - 1)) - 1);

// Calculate max raw value    ->>>>>> FORGOT ABOUT THIS??
constexpr double MAX_RAW = pow(2, MIC_BITS - 1);

float raw_ref;
float db_ref;
float gainChannel1;
uint8_t inputChannel = 1;
float temp_peak_raw;         // this is horrible because I need to figure out the q struct scope properly

Preferences preferences;
uint32_t lastQ = 0;
//size_t bytes_read = 0;
//size_t bytes_written = 0;
String ctrlBuf = "";        // incoming commands from faart
constexpr uint32_t queues_per_minute = (60 * SAMPLE_RATE) / SAMPLES_SHORT;      // how many 125ms periods will we read in 1 minute?
//
// declare Leq averages
MovingAverage<float>* LZeq1;
MovingAverage<float>* LAeq1;
MovingAverage<float>* LCeq1;

MovingAverage<float>* LZeq10;
MovingAverage<float>* LAeq10;
MovingAverage<float>* LCeq10;

double short_z_slow;
double short_a_slow;
double short_c_slow;

// Data we push to 'samples_queue'
struct sum_queue_t {
  // Sum of squares of mic samples, after Equalizer filter
  float sum_sqr_SPL;
  // Sum of squares of weighted mic samples
  float sum_sqr_a_weighted;
  float sum_sqr_c_weighted;
  float peak_c;
  float peak_z_raw;
  bool over;
  // Debug only, FreeRTOS ticks we spent processing the I2S data
  uint32_t proc_ticks;
};
QueueHandle_t samples_queue;

// buffer for incoming samples (in stereo). Mustbe in internal RAM because we are using DMA
float samplesIn[SAMPLES_SHORT * 2] __attribute__((aligned(4)));

// declare buffers for mono samples to analyze
float* samples;
float* samplesDump;

// float samples[SAMPLES_SHORT] __attribute__((aligned(4)));
// float samplesDump[SAMPLES_SHORT] __attribute__((aligned(4)));   //

SOS_IIR_Filter custom = {
  gain : 1.0,
sos : {
    { -1.0, 0.0, +0.9992, 0},
    { -1.0, 0.0, +0.9992, 0}
  }
};



void setup() {

  // construct new Leq averages. Have to do it here because we want to use PSRAM

  LZeq1 = new MovingAverage<float> (60, 0);
  LAeq1 = new MovingAverage<float> (60, 0);
  LCeq1 = new MovingAverage<float> (60, 0);

  LZeq10 = new MovingAverage<float> (10 * 60, 0);
  LAeq10 = new MovingAverage<float> (10 * 60, 0);
  LCeq10 = new MovingAverage<float> (10 * 60, 0);

  // and the same deal for samples

  samples = (float*)ps_malloc(sizeof(float) * SAMPLES_SHORT);  // <-- YES WE should be using sizeof()??
  samplesDump =  (float*)ps_malloc(sizeof(float) * SAMPLES_SHORT);   // <-- YES WE should be using sizeof()??

  ll.begin (115200, SERIAL_8N1, FAART_RX, FAART_TX);
 
  preferences.begin ("splthing", false);

  //  custom = SOS_IIR_Filter({
  //    gain: preferences.getDouble("coGain", 0.0),
  //    sos: {
  //          {preferences.getDouble("co1_1", 0.0), preferences.getDouble("co2_1", 0.0), preferences.getDouble("co3_1", 0.0), preferences.getDouble("co4_1", 0.0)},
  //          {preferences.getDouble("co1_2", 0.0), preferences.getDouble("co2_2", 0.0), preferences.getDouble("co3_2", 0.0), preferences.getDouble("co4_2", 0.0)}
  //        }
  //  });

  Wire.begin();
  ll.printf ("Init ADC...");
  if (!ADCInitSimple()) {
    ll.printf ("failed! halt\n");
    while (1) {}
  } else
    ll.printf ("OK\n");

  ll.printf ("DMA Banks : %i\n", DMA_BANKS);
  ll.printf ("DMA Bank Size : %i\n", DMA_BANK_SIZE);

  ll.printf ("Init i2s input...");
  if (!mic_i2s_init()) {
    ll.printf ("failed! halt\n");
    while (1) {}
  }
  ll.printf ("OK\n");
  ll.printf ("Init i2s monitor out...");
  if (!thru_i2s_init()) {
    ll.printf ("failed! halt\n");
    while (1) {}
  }
  ll.printf ("OK\n");
  ll.printf ("everything's good apparently... starting tasks\n");
  ll.printf ("MIC_OFFSET_DB=%.2f\n", MIC_OFFSET_DB);
  ll.printf ("MIC_REF_DB=%.2f\n", MIC_REF_DB);
  ll.printf ("MIC_REF_AMPL=%.2f\n", MIC_REF_AMPL);

  // this is wrong !!
  //  ll.printf ("Leq1 buffer: %i readings of %i bytes (total %i)\n", LZeq1.size(), sizeof(uint16_t), LZeq1.size()*sizeof(uint16_t));
  //  ll.printf ("Leq10 buffer: %i readings of %i bytes (total %i)\n", LZeq10.size(), sizeof(uint16_t), LZeq10.size()*sizeof(uint16_t));
  //
  
  ll.printf(" heap: total=%d, free=%d, used=%d\n", ESP.getHeapSize(), ESP.getFreeHeap(), (ESP.getHeapSize() - ESP.getFreeHeap()));
  ll.printf("PSRAM: total=%d, free=%d, used=%d\n", ESP.getPsramSize(), ESP.getFreePsram(), (ESP.getPsramSize() - ESP.getFreePsram()));

  recall();
  samples_queue = xQueueCreate(8, sizeof(sum_queue_t));

  xTaskCreate(mic_i2s_reader_task, "Mic I2S Reader", I2S_TASK_STACK, NULL, I2S_TASK_PRI, NULL);
  delay(200);
  xTaskCreate(spl_calc_task, "SPL Calculator", I2S_TASK_STACK, NULL, I2S_TASK_PRI, NULL);
}

void loop() {
  // deal with incoming faart comms

  if (ll.available() > 0) {
    char c = ll.read();
    if (c == '\n')
      processCommand();
    else
      ctrlBuf += c;
  }
  yield();
}

void spl_calc_task (void* parameter) {
  sum_queue_t q;
  uint32_t Leq_samples = 0;
  double LZeq_sum_sqr = 0;
  double LAeq_sum_sqr = 0;
  double LCeq_sum_sqr = 0;

  double Leq_dB = 0;
  float Leq_raw_avg;

  // Read sum of samaples, calculated by 'i2s_reader_task'
  while (xQueueReceive(samples_queue, &q, portMAX_DELAY)) {

    // Calculate dB values relative to raw_ref and adjust for microphone reference
    double short_RMS = sqrt(double(q.sum_sqr_SPL) / SAMPLES_SHORT);
    double short_SPL_dB = MIC_OFFSET_DB + db_ref + 20 * log10(short_RMS / raw_ref);
    double short_a_RMS = sqrt(double(q.sum_sqr_a_weighted) / SAMPLES_SHORT);
    double short_SPL_a_dB = MIC_OFFSET_DB + db_ref + 20 * log10(short_a_RMS / raw_ref);
    double short_c_RMS = sqrt(double(q.sum_sqr_c_weighted) / SAMPLES_SHORT);
    double short_SPL_c_dB = MIC_OFFSET_DB + db_ref + 20 * log10(short_c_RMS / raw_ref);

    // calculate slow
    if (short_SPL_dB > short_z_slow) {
      short_z_slow += (short_SPL_dB - short_z_slow) / 8;
    } else {
      short_z_slow -= 0.3625;
    }

    if (short_SPL_a_dB > short_a_slow) {
      short_a_slow += (short_SPL_a_dB - short_a_slow) / 8;
    } else {
      short_a_slow -= 0.3625;
    }

    if (short_SPL_c_dB > short_c_slow) {
      short_c_slow += (short_SPL_c_dB - short_c_slow) / 8;
    } else {
      short_c_slow -= 0.3625;   // dB/0.125sec fall off
    }

    // calculate peak

    double short_peak_c_dB = MIC_OFFSET_DB + db_ref + 20 * log10(q.peak_c / raw_ref);
    double short_peak_dBFS = 20 * log10(q.peak_z_raw / MAX_RAW); // ref is a 24 bit signed int minus the sign, so 2^23 is the ref ???

    // calculate 1 second L?eq

    LZeq_sum_sqr += q.sum_sqr_SPL;
    LAeq_sum_sqr += q.sum_sqr_a_weighted;
    LCeq_sum_sqr += q.sum_sqr_c_weighted;
    Leq_samples += SAMPLES_SHORT;

    if (Leq_samples >= SAMPLE_RATE * LEQ_PERIOD) {          // one second

      // Calculate one-second Leqs

      Leq_raw_avg = (float)sqrt(LZeq_sum_sqr / Leq_samples);
      LZeq1->push (Leq_raw_avg);
      LZeq10->push (Leq_raw_avg);

      Leq_raw_avg = (float)sqrt(LAeq_sum_sqr / Leq_samples);
      LAeq1->push (Leq_raw_avg);
      LAeq10->push (Leq_raw_avg);

      Leq_raw_avg = (float)sqrt(LCeq_sum_sqr / Leq_samples);
      LCeq1->push (Leq_raw_avg);
      LCeq10->push (Leq_raw_avg);

      //      ll.printf ("*raw1s=%f\n",Leq_raw_avg);

      LAeq_sum_sqr = 0;
      LCeq_sum_sqr = 0;
      LZeq_sum_sqr = 0;
      Leq_samples = 0;
    }

    // calculate dB for Leqs

    double short_LZeq1_dB = MIC_OFFSET_DB + db_ref + 20 * log10(LZeq1->get() / raw_ref);
    double short_LAeq1_dB = MIC_OFFSET_DB + db_ref + 20 * log10(LAeq1->get() / raw_ref);
    double short_LCeq1_dB = MIC_OFFSET_DB + db_ref + 20 * log10(LCeq1->get() / raw_ref);
    double short_LZeq10_dB = MIC_OFFSET_DB + db_ref + 20 * log10(LZeq10->get() / raw_ref);
    double short_LAeq10_dB = MIC_OFFSET_DB + db_ref + 20 * log10(LAeq10->get() / raw_ref);
    double short_LCeq10_dB = MIC_OFFSET_DB + db_ref + 20 * log10(LCeq10->get() / raw_ref);

    // send serial output to faart or microphonecalibrator

    // Peak
    ll.printf("*PeakC=%.1f\n", short_peak_c_dB);
    ll.printf("*PeakFS=%.1f\n", short_peak_dBFS);
    // SPL fast
    ll.printf("*SPLZF=%.1f\n*SPLAF=%.1f\n*SPLCF=%.1f\n", short_SPL_dB, short_SPL_a_dB, short_SPL_c_dB);
    // SPL slow

    ll.printf("*SPLZS=%.1f\n*SPLAS=%.1f\n*SPLCS=%.1f\n", short_z_slow, short_a_slow, short_c_slow);
    // L?eq1
    ll.printf("*LZeq1=%.1f\n*LAeq1=%.1f\n*LCeq1=%.1f\n", short_LZeq1_dB, short_LAeq1_dB, short_LCeq1_dB);
    // L?eq10
    ll.printf("*LZeq10=%.1f\n*LAeq10=%.1f\n*LCeq10=%.1f\n", short_LZeq10_dB, short_LAeq10_dB, short_LCeq10_dB);
    // microphone gain
    ll.printf("*gain=%.1f\n", gainChannel1);
    //  ll.printf ("time: %i\n",q.proc_ticks);
    ll.printf ("*HEAP=%i\n", heap_caps_get_free_size( MALLOC_CAP_8BIT));
    lastQ = millis();
  }
}


void mic_i2s_reader_task(void* parameter) {

  // Discard first block, microphone may have startup time (i.e. INMP441 up to 83ms)
  size_t bytes_read = 0;
  size_t bytes_written = 0;
  i2s_read(I2S_PORT, &samplesIn, 3000 * sizeof(int32_t), &bytes_read, portMAX_DELAY);



  while (true) {
    // Block and wait for microphone values from I2S
    //
    // Data is moved from DMA buffers to our 'samples' buffer by the driver ISR
    // and when there is requested ammount of data, task is unblocked
    //
    // Note: i2s_read does not care it is writing in float[] buffer, it will write
    //       integer values to the given address, as received from the hardware peripheral.
    i2s_read(I2S_PORT, &samplesIn, SAMPLES_SHORT * sizeof(SAMPLE_T) * 2, &bytes_read, portMAX_DELAY);

    // mirror everything to the THRU i2s port for montioring
    i2s_write (I2S_THRU_PORT, &samplesIn, bytes_read, &bytes_written, portMAX_DELAY);
    TickType_t start_tick = xTaskGetTickCount();

    //    ll.printf ("read %i (%i samples)\n",bytes_read, bytes_read/sizeof(SAMPLE_T));

    // Convert (including shifting) integer microphone values to floats,
    // with smp=0 we get input 1, smp=1 we get input 2??
    int smp;
    if (inputChannel == 2) smp = 0; else smp = 1;
    SAMPLE_T* int_samples = (SAMPLE_T*)&samplesIn;
    for (int i = 0; i < SAMPLES_SHORT; i++) {
      samples[i] = MIC_CONVERT(int_samples[smp]);
      smp += 2;
    }

    sum_queue_t q;

    // Apply mic equalization and calculate Z-weighted sum of squares,
    // writes filtered samples back to the same buffer
    //    q.sum_sqr_SPL = custom.filter(samples, samples, SAMPLES_SHORT);
    q.sum_sqr_SPL = custom.filter(samples, samples, SAMPLES_SHORT);
    // Apply weighting and calucate weigthed sum of squares but throw away the result into a dump
    q.sum_sqr_a_weighted = A_weighting.filter(samples, samplesDump, SAMPLES_SHORT);
    q.sum_sqr_c_weighted = C_weighting.filter(samples, samplesDump, SAMPLES_SHORT);

    q.peak_c = C_weighting.filter_peak (samples, samplesDump, SAMPLES_SHORT);

    q.peak_z_raw = 0;

    for (int i = 0; i < SAMPLES_SHORT; i++)
      if (samples[i] > q.peak_z_raw) q.peak_z_raw = samples[i];

    temp_peak_raw = q.peak_z_raw;

    // for the moment we count any max value sample as an over for this block.
    // eventually we should probably count contiguous samples.
    q.over = (q.peak_z_raw == OVER_VAL);

    // Debug only. Ticks we spent filtering and summing block of I2S data
    q.proc_ticks = xTaskGetTickCount() - start_tick;

    // Send the sums to FreeRTOS queue where main task will pick them up
    // and further calcualte decibel values (division, logarithms, etc...)
    xQueueSend(samples_queue, &q, portMAX_DELAY);
  }
}

//
// I2S Microphone sampling setup
//
bool mic_i2s_init() {
  // Setup I2S to sample mono channel for SAMPLE_RATE * SAMPLE_BITS
  // NOTE: Recent update to Arduino_esp32 (1.0.2 -> 1.0.3)
  //       seems to have swapped ONLY_LEFT and ONLY_RIGHT channels
  const i2s_config_t i2s_config = {
    .mode = i2s_mode_t(I2S_MODE_MASTER | I2S_MODE_RX), // Receive, not transfer
    .sample_rate = SAMPLE_RATE,                         //
    .bits_per_sample = i2s_bits_per_sample_t(SAMPLE_BITS), //i2s_bits_per_sample_t(SAMPLE_BITS), // could only get it to work with 32bits
    .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT, // use left channel
    .communication_format = i2s_comm_format_t(I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB),
    .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,     // Interrupt level 1
    .dma_buf_count = DMA_BANKS,                           // number of buffers
    .dma_buf_len = DMA_BANK_SIZE,                              // 8 samples per buffer (minimum)
    .use_apll = true,
    .tx_desc_auto_clear = false,
    .fixed_mclk = 0
  };

  // The pin   config as per the setup
  const i2s_pin_config_t pin_config = {
    .bck_io_num = I2S_SCK,   // Serial Clock (SCK)
    .ws_io_num = I2S_WS,    // Word Select (WS)
    .data_out_num = -1, // not used
    .data_in_num = I2S_SD   // Serial Data (SD)
  };

  esp_err_t rtn = i2s_driver_install(I2S_PORT, &i2s_config, 0, NULL);
  if (rtn != ESP_OK)
  {
    ll.printf ("i2s input driver failed=%d\n", rtn);
    return false;
  }

  i2s_set_pin(I2S_PORT, &pin_config);
  return true;
}

//
// I2S Microphone sampling setup
//
bool thru_i2s_init() {
  // Setup I2S to sample mono channel for SAMPLE_RATE * SAMPLE_BITS
  // NOTE: Recent update to Arduino_esp32 (1.0.2 -> 1.0.3)
  //       seems to have swapped ONLY_LEFT and ONLY_RIGHT channels
  const i2s_config_t i2s_thru_config = {
    .mode = i2s_mode_t(I2S_MODE_MASTER | I2S_MODE_TX), // Receive, not transfer
    .sample_rate = SAMPLE_RATE,                         //
    .bits_per_sample = I2S_BITS_PER_SAMPLE_32BIT,//i2s_bits_per_sample_t(SAMPLE_BITS), // could only get it to work with 32bits
    .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT, // use right channel
    .communication_format = i2s_comm_format_t(I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB),
    .intr_alloc_flags = 1 ,     // Interrupt
    .dma_buf_count = DMA_BANKS,                           // number of buffers
    .dma_buf_len = DMA_BANK_SIZE,                              // 8 samples per buffer (minimum)
    .use_apll = true,
    .tx_desc_auto_clear = false,
    .fixed_mclk = 0
  };

  // The pin config as per the setup
  const i2s_pin_config_t pin_config = {
    .bck_io_num = I2S_THRU_SCK,   // Serial Clock (SCK)
    .ws_io_num = I2S_THRU_WS,    // Word Select (WS)
    .data_out_num = I2S_THRU_SD, // Serial Data (SD)
    .data_in_num = I2S_PIN_NO_CHANGE   // not used
  };

  esp_err_t rtn = i2s_driver_install(I2S_THRU_PORT, &i2s_thru_config, 0, NULL);
  if (rtn != ESP_OK) {
    ll.printf ("i2s thru driver failed=%d\n", rtn);
    return false;
  }

  i2s_set_pin(I2S_THRU_PORT, &pin_config);
  return true;
}

void resetLeq() {
  // clear them Leq buffers
  LZeq1->rp();
  LAeq1->rp();
  LCeq1->rp();
  LZeq10->rp();
  LAeq10->rp();
  LCeq10->rp();
}


int StringSplit(String sInput, char cDelim, String sParams[], int iMaxParams)
{
  int iParamCount = 0;
  int iPosDelim, iPosStart = 0;

  do {
    // Searching the delimiter using indexOf()
    iPosDelim = sInput.indexOf(cDelim, iPosStart);
    if (iPosDelim > (iPosStart + 1)) {
      // Adding a new parameter using substring()
      sParams[iParamCount] = sInput.substring(iPosStart, iPosDelim);
      iParamCount++;
      // Checking the number of parameters
      if (iParamCount >= iMaxParams) {
        return (iParamCount);
      }
      iPosStart = iPosDelim + 1;
    }
  } while (iPosDelim >= 0);
  if (iParamCount < iMaxParams) {
    // Adding the last parameter as the end of the line
    sParams[iParamCount] = sInput.substring(iPosStart);
    iParamCount++;
  }

  return (iParamCount);
}

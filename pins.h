
// i2s connections to the tlv320adc6120
#define I2S_WS                33        // LRCLK
#define I2S_SCK               32        // BCLK
#define I2S_SD                15        // SDATA

// i2s connections to a monitoring device
#define I2S_THRU_WS           0        // LRCLK
#define I2S_THRU_SCK          14         // BCLK
#define I2S_THRU_SD           25        // SDATA

// serial connection to faart for logging and control
#define FAART_TX              2         // ESP32 to PC
#define FAART_RX              13         // PC to ESP32

//// GPS built into TTGO board
//#define GPS_RX                34        // GPS to ESP32
//#define GPS_TX                12        // ESP32 to GPS
//
//// MODEL_ID voltage divider ADC input
//#define MODEL_ID              35

# tlv320adc6120_test

this will eventually be merged into the final remote firmware along with all the GPS/LoRA  stuff. For now though, this is where the audio is happening.

Pins are set in ```pins.h```. Currently they are set for my perfboard prototype and are wrong for the PCB. Change them and [git rm](https://git-scm.com/docs/git-rm) this file so it won't get overwritten by subsequent pulls.

The main ADC uses I2S peripheral 0. A "thru" monitoring device can be connected as well.

This should be used with the faart or microphonecalibrator Processing applications.